package org.graphwalker.core.generator;

import org.graphwalker.core.algorithm.AStar;
import org.graphwalker.core.algorithm.AlgorithmException;
import org.graphwalker.core.algorithm.FloydWarshall;
import org.graphwalker.core.algorithm.MySearch;
import org.graphwalker.core.condition.ReachedStopCondition;
import org.graphwalker.core.machine.Context;
import org.graphwalker.core.model.Edge;
import org.graphwalker.core.model.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

public class MySearchPath extends PathGeneratorBase<ReachedStopCondition> {

    private static final Logger LOG = LoggerFactory.getLogger(MySearchPath.class);

    public MySearchPath(ReachedStopCondition stopCondition) {
        setStopCondition(stopCondition);
    }

    @Override
    public Context getNextStep() {
        Context context = super.getNextStep();
        Element currentElement = context.getCurrentElement();
        List<Element> elements = context.filter(context.getModel().getElements(currentElement));
        if (elements.isEmpty()) {
            LOG.error("currentElement: " + currentElement);
            LOG.error("context.getModel().getElements(): " + context.getModel().getElements());
            throw new NoPathFoundException(context.getCurrentElement());
        }
        Edge.RuntimeEdge target = null;
        int distance = Integer.MAX_VALUE;
        FloydWarshall floydWarshall = context.getAlgorithm(FloydWarshall.class);
        Set<Element> targetElements = getStopCondition().getTargetElements();
        for (Element element : targetElements) {
            if (element instanceof Edge.RuntimeEdge) {
                int edgeDistance = floydWarshall.getShortestDistance(currentElement, element);
                if (edgeDistance < distance) {
                    distance = edgeDistance;
                    target = (Edge.RuntimeEdge)element;
                }
            }
        }
        MySearch mySearch = context.getAlgorithm(MySearch.class);

        Element nextElement = mySearch.getNextElement(currentElement, target);
        context.setCurrentElement(nextElement);
        return context;
    }

    @Override
    public boolean hasNextStep() {
        return !getStopCondition().isFulfilled();
    }
}
