package org.graphwalker.core.generator;

import org.graalvm.polyglot.Value;
import org.graphwalker.core.algorithm.IntegratedSearch;
import org.graphwalker.core.condition.StopCondition;
import org.graphwalker.core.machine.Context;
import org.graphwalker.core.model.Action;
import org.graphwalker.core.model.Edge;
import org.graphwalker.core.model.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class IntegratedRandomPath extends PathGeneratorBase<StopCondition> {

    private static final Logger LOG = LoggerFactory.getLogger(IntegratedRandomPath.class);
    private final List<Edge.RuntimeEdge> unvisitedEdges = new ArrayList<>();
    private Edge.RuntimeEdge target = null;
    private int randomPenalty = 0;
    private boolean isGeneticMode = false;

    public IntegratedRandomPath(StopCondition stopCondition) {
        setStopCondition(stopCondition);
    }

    @Override
    public Context getNextStep() {
        Context context = super.getNextStep();
        Element currentElement = context.getCurrentElement();

        // Lúc đầu khởi tạo 1 list singleton chứa tất cả các cạnh
        if (unvisitedEdges.isEmpty()) {
            unvisitedEdges.addAll(context.getModel().getEdges());
            if (currentElement instanceof Edge.RuntimeEdge)
                unvisitedEdges.remove(currentElement);
        }

        if (isGeneticMode) {
            return runGeneticMode(context, currentElement);
        }

        return runRandomMode(context, currentElement);
    }

    private Context runRandomMode(Context context, Element currentElement) {
        // Lấy các element tiếp theo có thể đi được tại current Element
        List<Element> elements = context.filter(context.getModel().getElements(currentElement));

        // Nếu ko có element tiếp theo hợp lệ thì back thẳng về start Element
        if (elements.isEmpty()) {
            context.setCurrentElement(context.getStartElement());
            return context;
        }

        // Pick random 1 element tiếp theo
        Element nextElement = elements.get(SingletonRandomGenerator.nextInt(elements.size()));

        // Nếu element tiếp theo mà là edge thuộc unvisited edge
        // thì reset randomPenalty và xóa nextElement khỏi unvisitedEdges
        // otherwise randomPenalty++
        if (nextElement instanceof Edge.RuntimeEdge) {
            if (unvisitedEdges.contains(nextElement)) {
                unvisitedEdges.remove(nextElement);
                LOG.debug("\nGOOD nextElement: {}", nextElement.getName());
                randomPenalty = 0;
                return context.setCurrentElement(nextElement);
            }

            randomPenalty++;
            LOG.debug("\nBAD nextElement => current penalty = {}", randomPenalty);
            if (randomPenalty > 200) {
                isGeneticMode = true;
                LOG.debug("\nSWITCH TO GENETIC MODE");
                rollContextVariablesBackToInitialState();
                return context.setCurrentElement(context.getStartElement());
            }
        }

        return context.setCurrentElement(nextElement);
    }

    private Context runGeneticMode(Context context, Element currentElement) {
        if (target == null || currentElement.equals(target)) {
            target = unvisitedEdges.get(0);
            LOG.debug("New selected target is: {} - {}", target.getId(), target.getName());
        }

        Element nextElement = context.getAlgorithm(IntegratedSearch.class).getNextElement(currentElement, target);
        if (nextElement == null) {
            unvisitedEdges.remove(target);
            if (unvisitedEdges.isEmpty()) {
                throw new NoPathFoundException(currentElement);
            }
            target = unvisitedEdges.get(0);
            return context.setCurrentElement(context.getStartElement());
        }

        if (nextElement instanceof Edge.RuntimeEdge) {
            unvisitedEdges.remove(nextElement);
        }
        return context.setCurrentElement(nextElement);
    }

    @Override
    public boolean hasNextStep() {
        return !getStopCondition().isFulfilled();
    }

    private void rollContextVariablesBackToInitialState() {
        Context context = getContext();
        List<Action> initActions = context.getModel().getActions();
        for (Action initAction : initActions) {
            context.execute(initAction);
        }
    }
}
