package org.graphwalker.core.generator;

import org.graphwalker.core.algorithm.MyAStar;
import org.graphwalker.core.algorithm.MySearch;
import org.graphwalker.core.condition.StopCondition;
import org.graphwalker.core.machine.Context;
import org.graphwalker.core.model.Edge;
import org.graphwalker.core.model.Element;
import org.graphwalker.core.statistics.Profiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.graphwalker.core.common.Objects.isNotNull;
import static org.graphwalker.core.common.Objects.isNull;

public class MyGenerator extends PathGeneratorBase<StopCondition> {

  private static final Logger LOG = LoggerFactory.getLogger(MyGenerator.class);
  private final List<Edge.RuntimeEdge> elements = new ArrayList<>();
  private Edge.RuntimeEdge target = null;

  public MyGenerator(StopCondition stopCondition) {
    setStopCondition(stopCondition);
  }

  @Override
  public Context getNextStep() {
    Context context = super.getNextStep();
    Element currentElement = context.getCurrentElement();
    if (elements.isEmpty()) {
      elements.addAll(context.getModel().getEdges());
      if (currentElement instanceof Edge.RuntimeEdge)
        elements.remove(currentElement);
      Collections.shuffle(elements, SingletonRandomGenerator.random());
    }
    if (isNull(target) || (target != null && target.equals(currentElement))) {
      if (elements.isEmpty()) {
        throw new NoPathFoundException(currentElement);
      } else {
        orderElementsUnvisitedFirst(elements);
        target = elements.get(0);
        LOG.debug("New selected target is: {} - {}", target.getId(), target.getName());
      }
    }
    Element nextElement = context.getAlgorithm(MySearch.class).getNextElement(currentElement, target);
    if (isNull(nextElement)) {
        elements.clear();
        target = null;
        return context.setCurrentElement(context.getStartElement());
    }

    if (nextElement instanceof Edge.RuntimeEdge)
      elements.remove(nextElement);
    return context.setCurrentElement(nextElement);
  }

  private void orderElementsUnvisitedFirst(List<Edge.RuntimeEdge> elements) {
    final Context context = getContext();
    final Profiler profiler = context.getProfiler();
    if (isNotNull(profiler)) {
      elements.sort((a, b) -> Boolean.compare(profiler.isVisited(context, a), profiler.isVisited(context, b)));
    }
  }

  @Override
  public boolean hasNextStep() {
    return !getStopCondition().isFulfilled();
  }
}
