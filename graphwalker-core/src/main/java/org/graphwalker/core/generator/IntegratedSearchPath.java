package org.graphwalker.core.generator;

import org.graphwalker.core.algorithm.FloydWarshall;
import org.graphwalker.core.algorithm.IntegratedSearch;
import org.graphwalker.core.condition.ReachedStopCondition;
import org.graphwalker.core.machine.Context;
import org.graphwalker.core.model.Edge;
import org.graphwalker.core.model.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

public class IntegratedSearchPath extends PathGeneratorBase<ReachedStopCondition> {

    private static final Logger LOG = LoggerFactory.getLogger(MySearchPath.class);
    public IntegratedSearchPath(ReachedStopCondition stopCondition) {
        setStopCondition(stopCondition);
    }


    @Override
    public Context getNextStep() {
        Context context = super.getNextStep();
        Element currentElement = context.getCurrentElement();
        List<Element> elements = context.filter(context.getModel().getElements(currentElement));
        if (elements.isEmpty()) {
            LOG.error("currentElement: " + currentElement);
            LOG.error("context.getModel().getElements(): " + context.getModel().getElements());
            throw new NoPathFoundException(context.getCurrentElement());
        }

        Edge.RuntimeEdge target = null;
        int distance = Integer.MAX_VALUE;
        FloydWarshall floydWarshall = context.getAlgorithm(FloydWarshall.class);
        Set<Element> targetElements = getStopCondition().getTargetElements();
        for (Element element : targetElements) {
            if (element instanceof Edge.RuntimeEdge) {
                int edgeDistance = floydWarshall.getShortestDistance(currentElement, element);
                if (edgeDistance < distance) {
                    distance = edgeDistance;
                    target = (Edge.RuntimeEdge)element;
                }
            }
        }

        IntegratedSearch integratedSearch = context.getAlgorithm(IntegratedSearch.class);
        assert target != null;
        Element nextElement = integratedSearch.getNextElement(currentElement, target);
        if (nextElement == null) {
            throw new NoPathFoundException(currentElement);
        }
        context.setCurrentElement(nextElement);
        return context;
    }

    @Override
    public boolean hasNextStep() {
        return !getStopCondition().isFulfilled();
    }
}
