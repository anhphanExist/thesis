package org.graphwalker.core.generator;

import org.graphwalker.core.algorithm.IntegratedSearch;
import org.graphwalker.core.algorithm.MySearch;
import org.graphwalker.core.condition.StopCondition;
import org.graphwalker.core.machine.Context;
import org.graphwalker.core.model.Edge;
import org.graphwalker.core.model.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.graphwalker.core.common.Objects.isNull;

public class IntegratedGenerator extends PathGeneratorBase<StopCondition> {

    private static final Logger LOG = LoggerFactory.getLogger(IntegratedGenerator.class);
    // TODO: turn into queue
    private final List<Edge.RuntimeEdge> elements = new ArrayList<>();
    private Edge.RuntimeEdge target = null;

    public IntegratedGenerator(StopCondition stopCondition) {
        setStopCondition(stopCondition);
    }

    @Override
    public Context getNextStep() {
        Context context = super.getNextStep();
        Element currentElement = context.getCurrentElement();
        // TODO
        // Khởi tạo 1 list singleton chứa tất cả các cạnh
        if (elements.isEmpty()) {
            elements.addAll(context.getModel().getEdges());
            // Nếu current là 1 cạnh thì pop khỏi queue
            if (currentElement instanceof Edge.RuntimeEdge)
                elements.remove(currentElement);
        }

        // target = cạnh đứng đầu priority queue
        if (isNull(target) || (target != null && target.equals(currentElement))) {
            if (elements.isEmpty()) {
                throw new NoPathFoundException(currentElement);
            } else {
                target = elements.get(0);
                LOG.debug("New selected target is: {} - {}", target.getId(), target.getName());
            }
        }

        // next Element
        Element nextElement = context.getAlgorithm(IntegratedSearch.class).getNextElement(currentElement, target);
        if (nextElement == null) {
            elements.remove(target);
            if (elements.isEmpty()) {
                throw new NoPathFoundException(currentElement);
            }
            target = elements.get(0);
            nextElement = context.getStartElement();
        }
        if (nextElement instanceof Edge.RuntimeEdge)
            elements.remove(nextElement);
        return context.setCurrentElement(nextElement);
    }

    @Override
    public boolean hasNextStep() {
        return !getStopCondition().isFulfilled();
    }
}
