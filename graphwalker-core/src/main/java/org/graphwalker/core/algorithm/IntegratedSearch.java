package org.graphwalker.core.algorithm;

import org.graalvm.polyglot.Value;
import org.graphwalker.core.machine.Context;
import org.graphwalker.core.model.*;
import org.jenetics.*;
import org.jenetics.engine.Engine;
import org.jenetics.engine.EvolutionResult;
import org.jenetics.util.Factory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class IntegratedSearch implements Algorithm {

    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(IntegratedSearch.class);
    // Context
    private final Context context;
    private final org.graalvm.polyglot.Context polyglotContext;
    private final Edge.RuntimeEdge startElement;
    private final Vertex.RuntimeVertex startVertex;
    private final Set<String> contextVariables;
    private final Map<String, Value> initialStateOfContextVariables = new HashMap<>();
    private final ArrayList<EncodeGene> encodeGenes;
    private final int lcm;
    private int numberOfTransitions = 0;
    private ArrayList<Edge.RuntimeEdge> currentTransitionPath;
    private int currentIndexInTransitionPath = 0;
    private int failCounter = 0;
    private Edge.RuntimeEdge currentDestination;


    public IntegratedSearch(Context context) {
        this.context = context;
        this.polyglotContext = context.getExecutionEnvironment();
        this.startElement = (Edge.RuntimeEdge) context.getStartElement();
        this.startVertex = startElement.getTargetVertex();
        this.contextVariables = polyglotContext.getBindings("js").getMemberKeys()
            .stream()
            .filter(var -> !var.equals("JsonContext"))
            .collect(Collectors.toSet());
        for (String contextVariable : this.contextVariables) {
            this.initialStateOfContextVariables.put(contextVariable, polyglotContext.getBindings("js").getMember(contextVariable));
        }
        this.encodeGenes = encode();
        this.lcm = lcmOfEncode(encodeGenes);
        for (EncodeGene gene : encodeGenes) {

            gene.range = lcm / gene.numberOfOutEdges;
            this.numberOfTransitions += gene.numberOfOutEdges;
        }
    }

    // roll back các bindings
    private void rollContextVariablesBackToInitialState() {
        for (String contextVariable : contextVariables) {
            polyglotContext.getBindings("js").putMember(contextVariable, initialStateOfContextVariables.get(contextVariable));
        }
    }


    // Với mỗi destination, encode GA tìm path có trọng số nhỏ nhất đến transition (tổng trọng số của các transition trong chromosome)
    // execute path tìm đc, thấy khác path thì back ngay về start element
    public Element getNextElement(Element origin, Edge.RuntimeEdge destination) {


        // Nếu vừa mới duyệt xong destination cũ, sure origin ko thể là start nên back thẳng về start element
        if (!destination.equals(currentDestination) && !origin.equals(startElement)) {
            currentIndexInTransitionPath = 0;
            failCounter = 0;
            currentDestination = destination;
            LOG.debug("Vua moi duyet xong destination cu, sure origin ko the la start nen back thang ve start element");
            logStatus(origin, destination, startElement);
            return startElement;
        }


        if (origin instanceof Edge.RuntimeEdge) {
            // Nếu origin là start element do bị set lại hoặc mới bắt đầu một destination mới
            // thì chạy genetic bắt đầu tìm đường
            if (origin.equals(startElement)) {
                rollContextVariablesBackToInitialState();
                currentIndexInTransitionPath = 0;
                currentDestination = destination;
                Genotype<IntegerGene> result = runGeneticAlgorithm(lcm, numberOfTransitions, destination);
                currentTransitionPath = decode((IntegerChromosome) result.getChromosome());
            }

            // Otherwise, đang chạy dở trên testPath, chạy tiếp trên test Path,
            currentIndexInTransitionPath += 1;
            Edge.RuntimeEdge nextEdge = currentTransitionPath.get(currentIndexInTransitionPath);

            // Thực thi các guard của nextEdge
            boolean isNextEdgeGuardAvailable = !nextEdge.hasGuard() || polyglotContext.eval("js", nextEdge.getGuard().getScript()).asBoolean();
            if (isNextEdgeGuardAvailable) {
                if (nextEdge.equals(destination)) {
                    failCounter = 0;
                }
                Element nextElement = nextEdge.getSourceVertex();
                logStatus(origin, destination, nextElement);
                return nextElement;
            } else {
                // nếu phần tử phía trước không đi được, genetic ra test path mới chạy lại từ đầu trên destination đó, 10 lần như thế
                if (failCounter < 3) {
                    failCounter++;
                    Element nextElement = startElement;
                    logStatus(origin, destination, nextElement);
                    return nextElement;
                }
                // vẫn khôgn đi được, trả về null để chọn destination khác, coi như bỏ destination này
                failCounter = 0;
                logStatus(origin, destination, null);
                return null;
            }
        }

        // Otherwise, origin là vertex tức là đang chạy dở, chạy tiếp đến next Edge,
        Element nextElement = currentTransitionPath.get(currentIndexInTransitionPath);
        logStatus(origin, destination, nextElement);
        return nextElement;
    }

    /**
     * Evaluate a test path complexity for the selected transition
     *
     * @param testPath    generated test Path to evaluate with fitness function
     * @param destination current transition
     * @return penalty value
     */
    private int feasibilityMetric(ArrayList<Edge.RuntimeEdge> testPath, Edge.RuntimeEdge destination) {
        // local polyglot because they dont allow multi-threading
        org.graalvm.polyglot.Context localPolyglotContext = org.graalvm.polyglot.Context.create();
        for (Action action : context.getModel().getActions()) {
            localPolyglotContext.eval("js", action.getScript());
        }

        int result = 0;
        int indexOfDestinationInTestPath = testPath.indexOf(destination);

        if (indexOfDestinationInTestPath == -1) {
            return 500000;
        }

        for (int i = indexOfDestinationInTestPath; i >= 0; i--) {
            Edge.RuntimeEdge ti = testPath.get(i);
            if (!ti.hasGuard()) {
                continue;
            }

            Set<String> contextVariablesInTiGuard = contextVariables.stream().filter(contextVariable ->
                ti.getGuard().getScript().contains(contextVariable)).collect(Collectors.toSet());

            Map<String, Boolean> existAffectActionForVariableInTiGuardMap = new HashMap<>();
            for (String v : contextVariablesInTiGuard) {
                existAffectActionForVariableInTiGuardMap.put(v, false);
            }

            int j = i;
            while (j > 0) {
                j = j - 1;
                Edge.RuntimeEdge tj = testPath.get(j);

                if (formAffectPair(tj, ti)) {

                    if (formOpposePair(tj, ti)) {
                        return 500000;
                    }

                    for (String v : contextVariablesInTiGuard) {
                        if (haveOnlyConstantAssignment(tj, v)) {
                            existAffectActionForVariableInTiGuardMap.put(v, true);
                            result += penaltyConstant(tj, ti, v);
                            continue;
                        }

                        if (haveVariablesAssignment(tj, v)) {
                            existAffectActionForVariableInTiGuardMap.put(v, true);
                            result += penaltyAssignment(ti, v) +
                                checkTransitionDependencies(testPath, tj, j, v);
                            continue;
                        }

                        result += penaltyNoOp(ti, v);
                    }
                }
            }

            boolean isTiGuardAvailable = localPolyglotContext.eval("js", ti.getGuard().getScript()).asBoolean();

            // Kiểm tra nếu edge ti mà chưa mở mà test path không có action affect đủ variable của guard ti thì loại
            for (Map.Entry<String, Boolean> mapElement : existAffectActionForVariableInTiGuardMap.entrySet()) {
                if (!isTiGuardAvailable && !mapElement.getValue()) {
                    return 500000;
                }
            }
        }

        return result;
    }

    private static class Penalty {
        static String patternGuardEqual = "[\\s]*==[\\s]*";
        static String patternGuardLowerGreater = "[\\s]*[><][^=]";
        static String patternGuardLowerEqualGreaterEqual = "[\\s]*((?:<=)|(?:>=))";
        static String patternGuardNotEqual = "[\\s]*!=";

        static String patternGuardEqualConstant = "[\\s]*==[\\s]*([0-9(?:true)(?:false)]*)[\\s]*[^\\+\\-\\*\\/]";
        static String patternGuardLowerGreaterConstant = "[\\s]*[><][^=][\\s]*([0-9(?:true)(?:false)]*)[\\s]*[^\\+\\-\\*\\/]";
        static String patternGuardLowerEqualGreaterEqualConstant = "[\\s]*((?:<=)|(?:>=))[\\s]*([0-9(?:true)(?:false)]*)[\\s]*[^\\+\\-\\*\\/]";
        static String patternGuardNotEqualConstant = "\\s]*!=[\\s]*([0-9(?:true)(?:false)]*)[\\s]*[^\\+\\-\\*\\/]";

        static String patternGuardLowerLowerEqualConstant = "[\\s]*<=?[\\s]*([0-9]*)[\\s]*";
        static String patternGuardGreaterGreaterEqualConstant = "[\\s]*>=?[\\s]*([0-9]*)[\\s]*";
    }

    private int penaltyNoOp(Edge.RuntimeEdge ti, String v) {
        int result = 0;
        String guardScript = ti.getGuard().getScript();
        Pattern patternGuardEqual = Pattern.compile(v + Penalty.patternGuardEqual);
        Pattern patternGuardLowerGreater = Pattern.compile(v + Penalty.patternGuardLowerGreater);
        ;
        Pattern patternGuardLowerEqualGreaterEqual = Pattern.compile(v + Penalty.patternGuardLowerEqualGreaterEqual);
        Pattern patternGuardNotEqual = Pattern.compile(v + Penalty.patternGuardNotEqual);

        Pattern patternGuardEqualConstant = Pattern.compile(v + Penalty.patternGuardEqualConstant);
        Pattern patternGuardLowerGreaterConstant = Pattern.compile(v + Penalty.patternGuardLowerGreaterConstant);
        Pattern patternGuardLowerEqualGreaterEqualConstant = Pattern.compile(v + Penalty.patternGuardLowerEqualGreaterEqualConstant);
        Pattern patternGuardNotEqualConstant = Pattern.compile(v + Penalty.patternGuardNotEqualConstant);


        Matcher matcherEqual = patternGuardEqual.matcher(guardScript);
        Matcher matcherNotEqual = patternGuardNotEqual.matcher(guardScript);
        Matcher matcherLowerGreater = patternGuardLowerGreater.matcher(guardScript);
        Matcher matcherLowerEqualGreaterEqual = patternGuardLowerEqualGreaterEqual.matcher(guardScript);

        Matcher matcherEqualConst = patternGuardEqualConstant.matcher(guardScript);
        Matcher matcherNotEqualConst = patternGuardNotEqualConstant.matcher(guardScript);
        Matcher matcherLowerGreaterConst = patternGuardLowerGreaterConstant.matcher(guardScript);
        Matcher matcherLowerEqualGreaterEqualConst = patternGuardLowerEqualGreaterEqualConstant.matcher(guardScript);

        if (matcherEqual.find()) {
            if (matcherEqualConst.find()) {
                result += 40;
            } else {
                result += 16;
            }
        }

        if (matcherNotEqual.find()) {
            if (matcherNotEqualConst.find()) {
                result += 16;
            } else {
                result += 4;
            }
        }

        if (matcherLowerGreater.find()) {
            if (matcherLowerGreaterConst.find()) {
                result += 32;
            } else {
                result += 12;
            }
        }

        if (matcherLowerEqualGreaterEqual.find()) {
            if (matcherLowerEqualGreaterEqualConst.find()) {
                result += 24;
            } else {
                result += 8;
            }
        }

        return result;
    }

    private int penaltyAssignment(Edge.RuntimeEdge ti, String v) {
        int result = 0;
        String guardScript = ti.getGuard().getScript();
        Pattern patternGuardEqual = Pattern.compile(v + Penalty.patternGuardEqual);
        Pattern patternGuardLowerGreater = Pattern.compile(v + Penalty.patternGuardLowerGreater);
        ;
        Pattern patternGuardLowerEqualGreaterEqual = Pattern.compile(v + Penalty.patternGuardLowerEqualGreaterEqual);
        Pattern patternGuardNotEqual = Pattern.compile(v + Penalty.patternGuardNotEqual);

        Pattern patternGuardEqualConstant = Pattern.compile(v + Penalty.patternGuardEqualConstant);
        Pattern patternGuardLowerGreaterConstant = Pattern.compile(v + Penalty.patternGuardLowerGreaterConstant);
        Pattern patternGuardLowerEqualGreaterEqualConstant = Pattern.compile(v + Penalty.patternGuardLowerEqualGreaterEqualConstant);
        Pattern patternGuardNotEqualConstant = Pattern.compile(v + Penalty.patternGuardNotEqualConstant);


        Matcher matcherEqual = patternGuardEqual.matcher(guardScript);
        Matcher matcherNotEqual = patternGuardNotEqual.matcher(guardScript);
        Matcher matcherLowerGreater = patternGuardLowerGreater.matcher(guardScript);
        Matcher matcherLowerEqualGreaterEqual = patternGuardLowerEqualGreaterEqual.matcher(guardScript);

        Matcher matcherEqualConst = patternGuardEqualConstant.matcher(guardScript);
        Matcher matcherNotEqualConst = patternGuardNotEqualConstant.matcher(guardScript);
        Matcher matcherLowerGreaterConst = patternGuardLowerGreaterConstant.matcher(guardScript);
        Matcher matcherLowerEqualGreaterEqualConst = patternGuardLowerEqualGreaterEqualConstant.matcher(guardScript);

        if (matcherEqual.find()) {
            if (matcherEqualConst.find()) {
                result += 60;
            } else {
                result += 40;
            }
        }

        if (matcherNotEqual.find()) {
            if (matcherNotEqualConst.find()) {
                result += 24;
            } else {
                result += 16;
            }
        }

        if (matcherLowerGreater.find()) {
            if (matcherLowerGreaterConst.find()) {
                result += 48;
            } else {
                result += 32;
            }
        }

        if (matcherLowerEqualGreaterEqual.find()) {
            if (matcherLowerEqualGreaterEqualConst.find()) {
                result += 36;
            } else {
                result += 24;
            }
        }

        return result;
    }

    private int penaltyConstant(Edge.RuntimeEdge tj, Edge.RuntimeEdge ti, String v) {
        int result = 0;
        String guardScript = ti.getGuard().getScript();
        Set<String> actionScripts = tj.getActions().stream().map(Action::getScript).filter(script -> script.contains(v)).collect(Collectors.toSet());

        Pattern patternGuardEqual = Pattern.compile(v + Penalty.patternGuardEqual);
        Pattern patternGuardLowerGreater = Pattern.compile(v + Penalty.patternGuardLowerGreater);

        Pattern patternGuardLowerEqualGreaterEqual = Pattern.compile(v + Penalty.patternGuardLowerEqualGreaterEqual);
        Pattern patternGuardNotEqual = Pattern.compile(v + Penalty.patternGuardNotEqual);

        Pattern patternGuardEqualConstant = Pattern.compile(v + Penalty.patternGuardEqualConstant);
        Pattern patternGuardLowerGreaterConstant = Pattern.compile(v + Penalty.patternGuardLowerGreaterConstant);
        Pattern patternGuardLowerEqualGreaterEqualConstant = Pattern.compile(v + Penalty.patternGuardLowerEqualGreaterEqualConstant);
        Pattern patternGuardNotEqualConstant = Pattern.compile(v + Penalty.patternGuardNotEqualConstant);
        Pattern patternGuardGreaterGreaterEqualConstant = Pattern.compile(v + Penalty.patternGuardGreaterGreaterEqualConstant);
        Pattern patternGuardLowerLowerEqualConstant = Pattern.compile(v + Penalty.patternGuardLowerLowerEqualConstant);

        Matcher matcherEqual = patternGuardEqual.matcher(guardScript);
        Matcher matcherNotEqual = patternGuardNotEqual.matcher(guardScript);
        Matcher matcherLowerGreater = patternGuardLowerGreater.matcher(guardScript);
        Matcher matcherLowerEqualGreaterEqual = patternGuardLowerEqualGreaterEqual.matcher(guardScript);

        Matcher matcherEqualConst = patternGuardEqualConstant.matcher(guardScript);
        Matcher matcherNotEqualConst = patternGuardNotEqualConstant.matcher(guardScript);
        Matcher matcherLowerGreaterConst = patternGuardLowerGreaterConstant.matcher(guardScript);
        Matcher matcherLowerEqualGreaterEqualConst = patternGuardLowerEqualGreaterEqualConstant.matcher(guardScript);

        Matcher matcherGreaterGreaterEqualConst = patternGuardGreaterGreaterEqualConstant.matcher(guardScript);
        Matcher matcherLowerLowerEqualConst = patternGuardLowerLowerEqualConstant.matcher(guardScript);

        // guard var >, >= x, khởi tạo var < x, ưu tiên action tăng var
        if (matcherGreaterGreaterEqualConst.find()) {
            Pattern patternActionIncrement = Pattern.compile(v + "(?:\\+\\+)|"
                + v + "[\\s]*(?:(?:\\+=)|(?:=[\\s]*" + v + "[\\s]*\\+))[\\s]*([0-9]+)");
            int initialInteger = initialStateOfContextVariables.get(v).asInt();
            int constantInGuard = Integer.parseInt(matcherGreaterGreaterEqualConst.group(1));
            if (initialInteger < constantInGuard) {
                for (String actionScript : actionScripts) {
                    Matcher matcherActionIncrement = patternActionIncrement.matcher(actionScript);
                    if (matcherActionIncrement.find()) {
                        return result - 128;
                    }
                }
            }
        }

        // gaurd var <, <= x, khởi tạo var > x, ưu tiên action giảm var
        if (matcherLowerLowerEqualConst.find()) {
            Pattern patternActionDecrement = Pattern.compile(v + "(?:\\-\\-)|"
                + v + "[\\s]*(?:(?:\\-=)|(?:=[\\s]*" + v + "[\\s]*\\-))[\\s]*([0-9]+)");
            int initialInteger = initialStateOfContextVariables.get(v).asInt();
            int constantInGuard = Integer.parseInt(matcherLowerLowerEqualConst.group(1));
            if (initialInteger > constantInGuard) {
                for (String actionScript : actionScripts) {
                    Matcher matcherActionDecrement = patternActionDecrement.matcher(actionScript);
                    if (matcherActionDecrement.find()) {
                        return result - 128;
                    }
                }
            }
        }

        if (matcherEqual.find()) {
            if (!matcherEqualConst.find()) {
                result += 60;
            }
        }

        if (matcherNotEqual.find()) {
            if (!matcherNotEqualConst.find()) {
                result += 24;
            }
        }

        if (matcherLowerGreater.find()) {
            if (!matcherLowerGreaterConst.find()) {
                result += 48;
            }
        }

        if (matcherLowerEqualGreaterEqual.find()) {
            if (!matcherLowerEqualGreaterEqualConst.find()) {
                result += 36;
            }
        }

        return result;
    }


    private boolean haveVariablesAssignment(Edge.RuntimeEdge t, String v) {

        Pattern patternActionConstantAffectV = Pattern.compile(v + "(?:\\+\\+)|" +
            v + "(?:\\-\\-)|" +
            v + "[\\s]*[=\\*\\/\\+\\-]=?[\\s]*(?:" + v + "[\\+\\-\\*\\/])?[\\s]*([0-9(?:false)(?:true)]*)");

        // Lấy toàn bộ action có v trừ đi các action gán constant
        Set<String> actionsScriptInTjThatConstantAffectV = t.getActions().stream()
            .map(Action::getScript)
            .filter(script -> patternActionConstantAffectV.matcher(script).find())
            .collect(Collectors.toSet());

        Set<String> actionsScriptInTjThatAffectV = t.getActions().stream()
            .map(Action::getScript)
            .filter(script -> script.contains(v))
            .collect(Collectors.toSet());

        if (actionsScriptInTjThatAffectV.isEmpty()) {
            return false; // ko có action thì nghỉ đi
        }
        // nếu số lượng action affect v lớn hơn số lượng action chỉ affect constant vào v thì tức là có variable assignment
        return actionsScriptInTjThatConstantAffectV.size() < actionsScriptInTjThatAffectV.size();
    }

    private boolean haveOnlyConstantAssignment(Edge.RuntimeEdge t, String v) {
        Pattern patternActionConstantAffectV = Pattern.compile(v + "(?:\\+\\+)|" +
            v + "(?:\\-\\-)|" +
            v + "[\\s]*[=\\*\\/\\+\\-]=?[\\s]*(?:" + v + "[\\+\\-\\*\\/])?[\\s]*([0-9(?:false)(?:true)]*)");

        // Lấy toàn bộ action có v gán constant so sánh với toàn bộ action
        Set<String> actionsScriptInTjThatConstantAffectV = t.getActions().stream()
            .map(Action::getScript)
            .filter(script -> patternActionConstantAffectV.matcher(script).find())
            .collect(Collectors.toSet());

        Set<String> actionsScriptInTjThatAffectV = t.getActions().stream()
            .map(Action::getScript)
            .filter(script -> script.contains(v))
            .collect(Collectors.toSet());

        // nếu số lượng action affect v có tồn tại (lớn hơn 0) bằng hơn số lượng action chỉ affect constant vào v thì tức là chỉ có constant assignment
        if (actionsScriptInTjThatAffectV.isEmpty()) {
            return false; // ko có action thì nghỉ đi
        }
        return actionsScriptInTjThatConstantAffectV.size() == actionsScriptInTjThatAffectV.size();
    }


    private boolean formOpposePair(Edge.RuntimeEdge t, Edge.RuntimeEdge destination) {
        return opposeGuardGuard(t, destination) || opposeTransitionGuard(t, destination);
    }

    private boolean opposeGuardGuard(Edge.RuntimeEdge tj, Edge.RuntimeEdge ti) {
        String tiGuardScript = ti.getGuard().getScript();
        String tjGuardScript = tj.getGuard().getScript();

        if (tiGuardScript == null || tjGuardScript == null) {
            return false;
        }
        Set<String> contextVariablesInTiGuard = contextVariables.stream().filter(tiGuardScript::contains).collect(Collectors.toSet());

        Set<String> contextVariablesInBothGuard = contextVariablesInTiGuard.stream().filter(tjGuardScript::contains).collect(Collectors.toSet());


        for (String var : contextVariablesInBothGuard) {

            Pattern patternLowerGreaterNotEqualInteger = Pattern.compile(var + "[\\s]*(?:(?:!=)|(?:<[^=])|(?:>[^=]))[\\s]*([0-9]*)[\\s]*");
            Pattern patternEqualInteger = Pattern.compile(var + "[\\s]*==[\\s]*([0-9]*)[\\s]*");
            Pattern patternLowerInteger = Pattern.compile(var + "[\\s]*<[^=][\\s]*([0-9]*)[\\s]*");
            Pattern patternGreaterGreaterEqualInteger = Pattern.compile(var + "[\\s]*(?:(?:>=)|(?:>))[\\s]*([0-9]*)[\\s]*");
            Pattern patternGreaterInteger = Pattern.compile(var + "[\\s]*>[^=][\\s]*([0-9]*)[\\s]*");
            Pattern patternLowerLowerEqualInteger = Pattern.compile(var + "[\\s]*(?:(?:<=)|(?:<))[\\s]*([0-9]*)[\\s]*");
            Pattern patternBoolean = Pattern.compile(var + "[\\s]*(?:==)[\\s]*(true|false)[\\s]*");

            // trường hợp 1: 1 guard <, >, != x mà 1 guard == x
            Matcher matcherInTiPatternLowerGreaterNotEqualInteger = patternLowerGreaterNotEqualInteger.matcher(tiGuardScript);
            Matcher matcherInTjPatternEqualInteger = patternEqualInteger.matcher(tjGuardScript);
            if (matcherInTiPatternLowerGreaterNotEqualInteger.find() && matcherInTjPatternEqualInteger.find()) {
                String constantAssignInTi = matcherInTiPatternLowerGreaterNotEqualInteger.group(1);
                String constantAssignInTj = matcherInTjPatternEqualInteger.group(1);
                if (constantAssignInTj.equalsIgnoreCase(constantAssignInTi)) {
                    return true;
                }
            }

            Matcher matcherInTiPatternEqualInteger = patternEqualInteger.matcher(tiGuardScript);
            Matcher matcherInTjPatternLowerGreaterNotEqualInteger = patternLowerGreaterNotEqualInteger.matcher(tjGuardScript);
            if (matcherInTiPatternEqualInteger.find() && matcherInTjPatternLowerGreaterNotEqualInteger.find()) {
                String constantAssignInTi = matcherInTiPatternEqualInteger.group(1);
                String constantAssignInTj = matcherInTjPatternLowerGreaterNotEqualInteger.group(1);
                if (constantAssignInTj.equalsIgnoreCase(constantAssignInTi)) {
                    return true;
                }
            }

            // trường hợp 2: 2 guard == nhưng x khác nhau
            matcherInTjPatternEqualInteger = patternEqualInteger.matcher(tjGuardScript);
            matcherInTiPatternEqualInteger = patternEqualInteger.matcher(tiGuardScript);
            if (matcherInTjPatternEqualInteger.find() && matcherInTiPatternEqualInteger.find()) {
                String constantAssignInTi = matcherInTiPatternEqualInteger.group(1);
                String constantAssignInTj = matcherInTjPatternEqualInteger.group(1);
                if (!constantAssignInTi.equalsIgnoreCase(constantAssignInTj)) {
                    return true;
                }
            }


            // trường hợp 3: 1 guard < mà 1 guard >, >=
            Matcher matcherInTiPatternLowerInteger = patternLowerInteger.matcher(tiGuardScript);
            Matcher matcherInTjPatternGreaterGreaterEqualInteger = patternGreaterGreaterEqualInteger.matcher(tjGuardScript);
            if (matcherInTiPatternLowerInteger.find() && matcherInTjPatternGreaterGreaterEqualInteger.find()) {
                String constantAssignInTi = matcherInTiPatternLowerInteger.group(1);
                String constantAssignInTj = matcherInTjPatternGreaterGreaterEqualInteger.group(1);
                if (constantAssignInTj.equalsIgnoreCase(constantAssignInTi)) {
                    return true;
                }
            }

            Matcher matcherInTiPatternGreaterGreaterEqualInteger = patternGreaterGreaterEqualInteger.matcher(tiGuardScript);
            Matcher matcherInTjPatternLowerInteger = patternLowerInteger.matcher(tjGuardScript);
            if (matcherInTiPatternGreaterGreaterEqualInteger.find() && matcherInTjPatternLowerInteger.find()) {
                String constantAssignInTi = matcherInTiPatternGreaterGreaterEqualInteger.group(1);
                String constantAssignInTj = matcherInTjPatternLowerInteger.group(1);
                if (constantAssignInTj.equalsIgnoreCase(constantAssignInTi)) {
                    return true;
                }
            }

            // trường hợp 4: 1 guard > mà 1 guard <, <=
            Matcher matcherInTiPatternGreaterInteger = patternGreaterInteger.matcher(tiGuardScript);
            Matcher matcherInTjPatternLowerLowerEqualInteger = patternLowerLowerEqualInteger.matcher(tjGuardScript);
            if (matcherInTiPatternGreaterInteger.find() && matcherInTjPatternLowerLowerEqualInteger.find()) {
                String constantAssignInTi = matcherInTiPatternGreaterInteger.group(1);
                String constantAssignInTj = matcherInTjPatternLowerLowerEqualInteger.group(1);
                if (constantAssignInTj.equalsIgnoreCase(constantAssignInTi)) {
                    return true;
                }
            }

            Matcher matcherInTiPatternLowerLowerEqualInteger = patternLowerLowerEqualInteger.matcher(tiGuardScript);
            Matcher matcherInTjPatternGreaterInteger = patternGreaterInteger.matcher(tjGuardScript);
            if (matcherInTiPatternLowerLowerEqualInteger.find() && matcherInTjPatternGreaterInteger.find()) {
                String constantAssignInTi = matcherInTiPatternLowerLowerEqualInteger.group(1);
                String constantAssignInTj = matcherInTjPatternGreaterInteger.group(1);
                if (constantAssignInTj.equalsIgnoreCase(constantAssignInTi)) {
                    return true;
                }
            }

            // trường hợp 5: boolean, có thể bỏ qua
//            Matcher matcherInTiPatternBoolean = patternBoolean.matcher(tiGuardScript);
//            Matcher matcherInTjPatternBoolean = patternBoolean.matcher(tjGuardScript);
//            if (matcherInTiPatternBoolean.find() && matcherInTjPatternBoolean.find()) {
//                boolean constantAssignInTi = Boolean.parseBoolean(matcherInTiPatternBoolean.group(1));
//                boolean constantAssignInTj = Boolean.parseBoolean(matcherInTjPatternBoolean.group(1));
//                if (constantAssignInTj != constantAssignInTi) {
//                    return true;
//                }
//            }

            // trường hợp 6: guard của tj lại mạnh hơn ti nếu so với v tại state bắt đầu, ti <, <= x mà tj <, <= x - c
            if (matcherInTiPatternLowerLowerEqualInteger.find()) {
                int constantInTiGuard = Integer.parseInt(matcherInTiPatternLowerLowerEqualInteger.group(1));
                int initialStateOfVar = initialStateOfContextVariables.get(var).asInt();
                if (constantInTiGuard < initialStateOfVar) {
                    if (matcherInTjPatternLowerLowerEqualInteger.find()) {
                        int constantInTjGuard = Integer.parseInt(matcherInTjPatternLowerLowerEqualInteger.group(1));
                        if (constantInTjGuard < constantInTiGuard) {
                            return true;
                        }
                    }
                }
            }

            // trường hợp 7: guard của ti >, >= x mà tj >, >= x + c
            if (matcherInTiPatternGreaterGreaterEqualInteger.find()) {
                int constantInTiGuard = Integer.parseInt(matcherInTiPatternGreaterGreaterEqualInteger.group(1));
                int initialStateOfVar = initialStateOfContextVariables.get(var).asInt();
                if (constantInTiGuard > initialStateOfVar) {
                    if (matcherInTjPatternGreaterGreaterEqualInteger.find()) {
                        int constantInTjGuard = Integer.parseInt(matcherInTjPatternGreaterGreaterEqualInteger.group(1));
                        if (constantInTjGuard > constantInTiGuard) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean opposeTransitionGuard(Edge.RuntimeEdge tj, Edge.RuntimeEdge ti) {
        // if t has op(v-c), destination has g(v-c) (c = c) but g is { >, <, != }
        String tiGuardScript = ti.getGuard().getScript();

        Set<String> contextVariablesInTiGuard = contextVariables.stream().filter(tiGuardScript::contains).collect(Collectors.toSet());

        Set<String> actionsScriptInTjThatAffectTiGuard = tj.getActions().stream()
            .map(Action::getScript)
            .filter(script -> contextVariablesInTiGuard.stream().anyMatch(script::contains))
            .collect(Collectors.toSet());

        for (String var : contextVariablesInTiGuard) {
            Pattern patternActionBoolean = Pattern.compile(var + "[\\s]*=[\\s]*(true|false)[\\s]*");
            Pattern patternGuardBoolean = Pattern.compile(var + "[\\s]*(?:==)[\\s]*(true|false)[\\s]*");
            Pattern patternActionEqualInteger = Pattern.compile(var + "[\\s]*=[\\s]*([0-9]*)[\\s]*");
            Pattern patternGuardEqualInteger = Pattern.compile(var + "[\\s]*==[\\s]*([0-9]*)[\\s]*");
            Pattern patternGuardLowerInteger = Pattern.compile(var + "[\\s]*<[^=][\\s]*([0-9]*)[\\s]*");
            Pattern patternGuardLowerLowerEqualInteger = Pattern.compile(var + "[\\s]*(?:(?:<=)|(?:<))[\\s]*([0-9]*)[\\s]*");
            Pattern patternGuardGreaterInteger = Pattern.compile(var + "[\\s]*>[^=][\\s]*([0-9]*)[\\s]*");
            Pattern patternGuardGreaterGreaterEqualInteger = Pattern.compile(var + "[\\s]*(?:(?:>=)|(?:>))[\\s]*([0-9]*)[\\s]*");
            Pattern patternGuardNotEqualInteger = Pattern.compile(var + "[\\s]*(?:!=)[\\s]*([0-9]*)[\\s]*");
            Pattern patternGuardLowerGreaterNotEqualInteger = Pattern.compile(var + "[\\s]*(?:(?:!=)|(?:<[^=])|(?:>[^=]))[\\s]*([0-9]*)[\\s]*");
            Pattern patternActionIncrement = Pattern.compile(var + "(?:\\+\\+)|"
                + var + "[\\s]*(?:(?:\\+=)|(?:=[\\s]*" + var + "[\\s]*\\+))[\\s]*([0-9]+)");
            Pattern patternActionDecrement = Pattern.compile(var + "(?:\\-\\-)|"
                + var + "[\\s]*(?:(?:\\-=)|(?:=[\\s]*" + var + "[\\s]*\\-))[\\s]*([0-9]+)");

            for (String actionScript : actionsScriptInTjThatAffectTiGuard) {
                // Action Gán boolean đối lập, guard == boolean
                Matcher matcherInTjActionBoolean = patternActionBoolean.matcher(actionScript);
                if (matcherInTjActionBoolean.find()) {
                    boolean constantAssignInTjAction = Boolean.parseBoolean(matcherInTjActionBoolean.group(1));
                    Matcher matcherInTiGuardBoolean = patternGuardBoolean.matcher(tiGuardScript);
                    if (matcherInTiGuardBoolean.find()) {
                        boolean constantAssignInTiGuard = Boolean.parseBoolean(matcherInTiGuardBoolean.group(1));
                        if (constantAssignInTjAction != constantAssignInTiGuard) {
                            return true;
                        }
                    }
                }

                // Action gán integer
                Matcher matcherInTjActionInteger = patternActionEqualInteger.matcher(actionScript);
                if (matcherInTjActionInteger.find()) {
                    String constantAssignInTjAction = matcherInTjActionInteger.group(1);


                    // Action gán integer, guard == integer khác
                    Matcher matcherInTiGuardEqualInteger = patternGuardEqualInteger.matcher(tiGuardScript);
                    if (matcherInTiGuardEqualInteger.find()) {
                        String constantAssignInTiGuard = matcherInTiGuardEqualInteger.group(1);
                        if (!constantAssignInTjAction.equalsIgnoreCase(constantAssignInTiGuard)) {
                            return true;
                        }
                    }

                    // Action gán integer x, guard !=, <, > x đó
                    Matcher matcherInTiGuardLowerGreaterNotEqualInteger = patternGuardLowerGreaterNotEqualInteger.matcher(tiGuardScript);
                    if (matcherInTiGuardLowerGreaterNotEqualInteger.find()) {
                        String constantAssignInTiGuard = matcherInTiGuardLowerGreaterNotEqualInteger.group(1);
                        if (constantAssignInTjAction.equalsIgnoreCase(constantAssignInTiGuard)) {
                            return true;
                        }
                    }

                    // Action gán y > x, guard <,<= x
                    Matcher matcherInTiGuardLowerLowerEqualInteger = patternGuardLowerLowerEqualInteger.matcher(tiGuardScript);
                    if (matcherInTiGuardLowerLowerEqualInteger.find()) {
                        int constantAssignInTiGuard = Integer.parseInt(matcherInTiGuardLowerLowerEqualInteger.group(1));
                        if (Integer.parseInt(constantAssignInTjAction) > constantAssignInTiGuard) {
                            return true;
                        }
                    }


                    // Action gán y < x, guard >, >= x
                    Matcher matcherInTiGuardGreaterGreaterEqualInteger = patternGuardGreaterGreaterEqualInteger.matcher(tiGuardScript);
                    if (matcherInTiGuardGreaterGreaterEqualInteger.find()) {
                        int constantAssignInTiGuard = Integer.parseInt(matcherInTiGuardGreaterGreaterEqualInteger.group(1));
                        if (Integer.parseInt(constantAssignInTjAction) < constantAssignInTiGuard) {
                            return true;
                        }
                    }

                    // Action gán y >= x, guard < x
                    Matcher matcherInTiGuardLowerInteger = patternGuardLowerInteger.matcher(tiGuardScript);
                    if (matcherInTiGuardLowerInteger.find()) {
                        int constantAssignInTiGuard = Integer.parseInt(matcherInTiGuardLowerInteger.group(1));
                        if (Integer.parseInt(constantAssignInTjAction) >= constantAssignInTiGuard) {
                            return true;
                        }
                    }

                    // Action gán y <= x, guard > x
                    Matcher matcherInTiGuardGreaterInteger = patternGuardGreaterInteger.matcher(tiGuardScript);
                    if (matcherInTiGuardGreaterInteger.find()) {
                        int constantAssignInTiGuard = Integer.parseInt(matcherInTiGuardGreaterInteger.group(1));
                        if (Integer.parseInt(constantAssignInTjAction) <= constantAssignInTiGuard) {
                            return true;
                        }
                    }
                }

                // Action tăng integer,mà guard <, <= x và khởi đầu var >= x
                Matcher matcherInTjActionIntegerIncrement = patternActionIncrement.matcher(actionScript);
                if (matcherInTjActionIntegerIncrement.find()) {
                    Matcher matcherInTiGuardLowerLowerEqualInteger = patternGuardLowerLowerEqualInteger.matcher(tiGuardScript);
                    if (matcherInTiGuardLowerLowerEqualInteger.find()) {
                        int constantAssignInTiGuard = Integer.parseInt(matcherInTiGuardLowerLowerEqualInteger.group(1));
                        int initialStateOfVar = initialStateOfContextVariables.get(var).asInt();
                        if (initialStateOfVar >= constantAssignInTiGuard) {
                            return true;
                        }
                    }
                }

                // Action giảm integer, mà guard >, >= x và khởi đầu var <= x
                Matcher matcherInTjActionIntegerDecrement = patternActionDecrement.matcher(actionScript);
                if (matcherInTjActionIntegerDecrement.find()) {
                    Matcher matcherInTiGuardGreaterGreaterEqualInteger = patternGuardGreaterGreaterEqualInteger.matcher(tiGuardScript);
                    if (matcherInTiGuardGreaterGreaterEqualInteger.find()) {
                        int constantAssignInTiGuard = Integer.parseInt(matcherInTiGuardGreaterGreaterEqualInteger.group(1));
                        int initialStateOfVar = initialStateOfContextVariables.get(var).asInt();
                        if (initialStateOfVar <= constantAssignInTiGuard) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean formAffectPair(Edge.RuntimeEdge t, Edge.RuntimeEdge destination) {
        if (!destination.hasGuard() || !t.hasActions()) {
            return false;
        }

        Set<String> contextVariablesInTiGuard = contextVariables.stream().filter(contextVariable ->
            destination.getGuard().getScript().contains(contextVariable)).collect(Collectors.toSet());

        return t.getActions().stream().anyMatch(action ->
            contextVariablesInTiGuard.stream().anyMatch(var -> action.getScript().contains(var)));
    }

    /**
     * Traceback a flow dependence on variable v
     *
     * @param tj tj
     * @param j  index of affecting transition
     * @param v  related variable in pair (affectingTransition, destination)
     * @return penalty value
     */
    private int checkTransitionDependencies(ArrayList<Edge.RuntimeEdge> testPath,
                                            Edge.RuntimeEdge tj, int j, String v) {
        int result = 0;
        boolean found = false;

        int p = j;
        do {
            p = p - 1;
            Edge.RuntimeEdge tp = testPath.get(p);

            if (formActionPair(tp, tj, v)) {
                Set<Action> actionsInTjThatAffectV = tj.getActions().stream()
                    .filter(action -> action.getScript().matches(v + "[\\s]*[\\*\\/\\+\\-]{0,1}=|" + v + "[\\+][\\+]|" + v + "[\\-][\\-]"))
                    .collect(Collectors.toSet());

                Set<String> contextVariablesInTjThatAffectV = contextVariables.stream()
                    .filter(var -> !var.equals(v) && actionsInTjThatAffectV.stream().anyMatch(action -> action.getScript().contains(var)))
                    .collect(Collectors.toSet());

                for (String v1 : contextVariablesInTjThatAffectV) {
                    if (haveOnlyConstantAssignment(tp, v1)) {
                        result += 20;
                        found = true;
                    }

                    if (haveVariablesAssignment(tp, v1)) {
                        result += 40 + checkTransitionDependencies(testPath, tp, p, v1);
                        found = true;
                    }
                }
            }
        } while (p > 0);

        if (found)
            return result;
        return result + 60;
    }

    private boolean formActionPair(Edge.RuntimeEdge tp, Edge.RuntimeEdge tj, String v) {
        Set<Action> actionsInTjThatAffectV = tj.getActions().stream()
            .filter(action -> action.getScript().matches(v + "(?:\\+\\+)|" +
                v + "(?:\\-\\-)|" +
                v + "[\\s]*[=\\*\\/\\+\\-]=?[\\s]*(?:" + v + "[\\+\\-\\*\\/])?([0-9(?:false)(?:true)]*)"))
            .collect(Collectors.toSet());

        Set<String> contextVariablesInTjThatAffectV = contextVariables.stream()
            .filter(var -> !var.equals(v) && actionsInTjThatAffectV.stream().anyMatch(action -> action.getScript().contains(var)))
            .collect(Collectors.toSet());

        Set<Action> actionsInTpAffectContextVariablesThatAffectVInTj = tp.getActions().stream()
            .filter(action -> contextVariablesInTjThatAffectV.stream()
                .anyMatch(var -> action.getScript().matches(var + "(?:\\+\\+)|" +
                    var + "(?:\\-\\-)|" +
                    var + "[\\s]*[=\\*\\/\\+\\-]=?[\\s]*(?:" + var + "[\\+\\-\\*\\/])?([0-9(?:false)(?:true)]*)")))
            .collect(Collectors.toSet());

        return !actionsInTpAffectContextVariablesThatAffectVInTj.isEmpty();
    }

    private class EncodeGene {
        private Vertex.RuntimeVertex vertex;
        private int index;
        private int numberOfOutEdges;
        private int range;

        private EncodeGene(Vertex.RuntimeVertex vertex, int index) {
            this.vertex = vertex;
            this.index = index;
            this.numberOfOutEdges = context.getModel().getOutEdges(vertex).size();
        }
    }

    public static int lcmOfArrayElements(int[] element_array) {
        int lcm_of_array_elements = 1;
        int divisor = 2;

        while (true) {
            int counter = 0;
            boolean divisible = false;

            for (int i = 0; i < element_array.length; i++) {

                // lcm_of_array_elements (n1, n2, ... 0) = 0.
                // For negative number we convert into
                // positive and calculate lcm_of_array_elements.

                if (element_array[i] == 0) {
                    return 0;
                } else if (element_array[i] < 0) {
                    element_array[i] = element_array[i] * (-1);
                }
                if (element_array[i] == 1) {
                    counter++;
                }

                // Divide element_array by devisor if complete
                // division i.e. without remainder then replace
                // number with quotient; used for find next factor
                if (element_array[i] % divisor == 0) {
                    divisible = true;
                    element_array[i] = element_array[i] / divisor;
                }
            }

            // If divisor able to completely divide any number
            // from array multiply with lcm_of_array_elements
            // and store into lcm_of_array_elements and continue
            // to same divisor for next factor finding.
            // else increment divisor
            if (divisible) {
                lcm_of_array_elements = lcm_of_array_elements * divisor;
            } else {
                divisor++;
            }

            // Check if all element_array is 1 indicate
            // we found all factors and terminate while loop.
            if (counter == element_array.length) {
                return lcm_of_array_elements;
            }
        }
    }

    private int lcmOfEncode(ArrayList<EncodeGene> encodeGenes) {
        return lcmOfArrayElements(encodeGenes.stream().mapToInt(gene -> gene.numberOfOutEdges).toArray());
    }

    private ArrayList<EncodeGene> encode() {
        List<Vertex.RuntimeVertex> vertices = context.getModel().getVertices();

        ArrayList<EncodeGene> encodeGenes = new ArrayList<>();
        EncodeGene startGene = new EncodeGene(startVertex, 1);
        // Loại bỏ start Element khỏi startGene nên trừ 1 numberOfOutEdges
        startGene.numberOfOutEdges--;
        encodeGenes.add(startGene);
        vertices = vertices.stream().filter(vertex -> !vertex.equals(startVertex)).collect(Collectors.toList());
        if (!vertices.isEmpty()) {
            for (int i = 0; i < vertices.size(); i++) {
                encodeGenes.add(new EncodeGene(vertices.get(i), i + 2));
            }
        }
        return encodeGenes;
    }

    private Integer fitness(Genotype<IntegerGene> lt, Edge.RuntimeEdge destination) {
        int result;
        IntegerChromosome lc = lt.getChromosome().as(IntegerChromosome.class);
        result = feasibilityMetric(decode(lc), destination);
        return result;
    }

    private Genotype<IntegerGene> runGeneticAlgorithm(int lcm, int numberOfTransitions, Edge.RuntimeEdge destination) {
        int chromosomeLength = numberOfTransitions;
        IntegerChromosome startChromosome = IntegerChromosome.of(1, lcm, chromosomeLength);
        Factory<Genotype<IntegerGene>> gtf = Genotype.of(startChromosome);
        Engine<IntegerGene, Integer> engine = Engine
            .builder(lt -> fitness(lt, destination), gtf)
            .populationSize(100)
            .optimize(Optimize.MINIMUM)
            .build();
        return engine
            .stream()
            .limit(1000)
            .collect(EvolutionResult.toBestGenotype());
    }


    private ArrayList<Edge.RuntimeEdge> decode(IntegerChromosome chromosome) {
        EncodeGene currentEncodeGene = encodeGenes.get(0);
        ArrayList<Edge.RuntimeEdge> testPath = new ArrayList<>();
        testPath.add(startElement);

        for (int i = 0; i < chromosome.length(); i++) {
            int geneValue = chromosome.getGene(i).intValue();
            Edge.RuntimeEdge nextEdge = decideEdge(geneValue, currentEncodeGene);
            testPath.add(nextEdge);

            Optional<EncodeGene> optionalNextGene = encodeGenes.stream().filter(gene -> gene.vertex.equals(nextEdge.getTargetVertex())).findFirst();
            if (!optionalNextGene.isPresent())
                throw new AlgorithmException("Decide Invalid Edge");
            currentEncodeGene = optionalNextGene.get();
        }

        return testPath;
    }

    private Edge.RuntimeEdge decideEdge(int geneValue, EncodeGene currentEncodeGene) {

        int range = currentEncodeGene.range;
        int transitionIndex = geneValue % range != 0 ? geneValue / range : geneValue / range - 1;
        if (currentEncodeGene.vertex.equals(startVertex)) {
            List<Edge.RuntimeEdge> outEdgeWithoutStartElement = context.getModel().getOutEdges(currentEncodeGene.vertex)
                .stream().filter(edge -> !edge.equals(startElement)).collect(Collectors.toList());
            return outEdgeWithoutStartElement.get(transitionIndex);

        }
        return context.getModel().getOutEdges(currentEncodeGene.vertex).get(transitionIndex);
    }

    private void logStatus(Element origin, Edge.RuntimeEdge destination, Element nextElement) {
        String currentDestinationName = currentDestination != null ? currentDestination.getName() : null;
        String nextElementName = nextElement != null ? nextElement.getName() : null;
        String transitionPathName = Arrays.toString(currentTransitionPath.stream().map(RuntimeBase::getName).toArray());
        LOG.debug("Current IntegratedSearch status: \n" +
                "- origin is {} \n" +
                "- destination is {} \n" +
                "- currentDestination is {} \n" +
                "- failCounter is {} \n" +
                "- currentIndexInTransitionPath is {} \n" +
                "- numberOfTransitions is {} \n" +
                "- lcm is {} \n" +
                "- currentTransitionPath is {} \n" +
                "- nextElement is {} ",
            origin.getName(), destination.getName(), currentDestinationName, failCounter,
            currentIndexInTransitionPath, numberOfTransitions,
            lcm, transitionPathName, nextElementName);
    }

}
