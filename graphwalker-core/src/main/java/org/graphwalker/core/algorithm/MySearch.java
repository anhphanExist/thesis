package org.graphwalker.core.algorithm;

import org.graalvm.polyglot.Value;
import org.graphwalker.core.machine.Context;
import org.graphwalker.core.model.Action;
import org.graphwalker.core.model.Edge;
import org.graphwalker.core.model.Element;
import org.graphwalker.core.model.Guard;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class MySearch implements Algorithm {
    // Logger
    private static final Logger LOG = LoggerFactory.getLogger(MySearch.class);
    // Context
    private final Context context;
    private final org.graalvm.polyglot.Context polyglotContext;

    // Constant Affect Matrix Map + floydWarshall machine
    private final Map<Edge.RuntimeEdge, ArrayList<Edge.RuntimeEdge>> affectMatrixMap;
    private final AStar aStar;

    // Current State of Search Machine
    private boolean isTraversingToAnything = false;
    private boolean isTraversingToAffectNode = false;

    private Edge.RuntimeEdge currentDestination = null;
    private Edge.RuntimeEdge currentAffectNode = null;
    private int currentAffectNodeIndex = 0;
    private int penalty = 0;

    public MySearch(Context context) {
        this.context = context;
        polyglotContext = context.getExecutionEnvironment();
        affectMatrixMap = createAffectMatrix(context.getModel().getElements());
        aStar = context.getAlgorithm(AStar.class);
    }

    private Map<Edge.RuntimeEdge, ArrayList<Edge.RuntimeEdge>> createAffectMatrix(List<Element> elements) {

        Map<Edge.RuntimeEdge, ArrayList<Edge.RuntimeEdge>> result = new HashMap<>();
        Set<String> contextVariables = polyglotContext.getBindings("js").getMemberKeys();

        for (Element element : elements) {

            if (element instanceof Edge.RuntimeEdge) {
                ArrayList<Edge.RuntimeEdge> affectNodes = new ArrayList<>();
                for (Element possibleAffectElement : elements) {

                    if (possibleAffectElement instanceof Edge.RuntimeEdge) {
                        if (((Edge.RuntimeEdge) element).hasGuard() && possibleAffectElement.hasActions()) {

                            Set<String> contextVariablesInGuard = contextVariables.stream().filter(contextVariable ->
                                ((Edge.RuntimeEdge) element).getGuard().getScript().contains(contextVariable)).collect(Collectors.toSet());

                            Set<String> contextVariableInGuardAndAction = contextVariablesInGuard.stream().filter(contextVariable ->
                                possibleAffectElement.getActions().stream().anyMatch(action ->
                                    contextVariablesInGuard.stream().anyMatch(var -> action.getScript().contains(var))
                                )
                            ).collect(Collectors.toSet());

                            if (!contextVariableInGuardAndAction.isEmpty())
                                affectNodes.add((Edge.RuntimeEdge) possibleAffectElement);
                        }
                    }
                }

                result.put((Edge.RuntimeEdge) element, affectNodes);
            }

        }
        return result;
    }

    // Lấy các affect Node của destination, rồi tìm đường từ các affect Node đến edge destination
    // Chưa cần đệ quy
    // Từ affect Node đi đường ngắn nhất đến destination
    // chuyển affect node nếu k reach được, hết affect node thì kẹt
    // Ongoing 1: Nếu next element mà bằng với element hiện tại, tăng penalty, penalty đủ lớn thì chuyển sang dùng affectNode khác
    // Ongoing 2: Nếu tất cả affectNode đều chạm penalty, chuyển sang đệ quy đi đến affect Node của affect Node
    // Ongoing 3: Nếu đệ quy không được thì signal đổi destination, loại destination khỏi danh sách cần duyệt luôn
    public Element getNextElement(Element origin, Edge.RuntimeEdge destination) {

        // Nếu destination này là mới, chuyển sang chế độ chạy mới
        if (!isTraversingToAnything) {
            currentDestination = destination;
            isTraversingToAnything = true;

            // Nếu destination không có guard, chạy BFS như bthg, YÊU CẦU đồ thị phải specify chuẩn
            if (!destination.hasGuard()) {
                Element nextElement = aStar.getNextElement(origin, currentDestination);
                logStatus(origin, destination, nextElement);
                return nextElement;
            }

            // Nếu destination mới có guard, bắt đầu chạy đến affect Node
            isTraversingToAffectNode = true;
            ArrayList<Edge.RuntimeEdge> affectNodes = affectMatrixMap.get(currentDestination);
            currentAffectNode = affectNodes.get(currentAffectNodeIndex);
            Element nextElement = aStar.getNextElement(origin, currentAffectNode);

            // Nếu next Element là destination
            if (nextElement.equals(currentDestination)) {
                return WhenNextElementIsDestination(origin, destination, nextElement);
            }

            // Nếu nextElement là current affectNode
            if (nextElement.equals(currentAffectNode)) {
                return WhenNextElementIsCurrentAffectNode(origin, destination, affectNodes, nextElement);
            }

            // Tiếp tục chạy đến affectNode
            logStatus(origin, destination, nextElement);
            return nextElement;
        }

        // Nếu đang traverse đến affect Node, tiếp tục duyệt đến affect Node
        if (isTraversingToAffectNode) {
            ArrayList<Edge.RuntimeEdge> affectNodes = affectMatrixMap.get(currentDestination);
            Element nextElement = aStar.getNextElement(origin, currentAffectNode);

            // Nếu nextElement là destination
            if (nextElement.equals(currentDestination)) {
                return WhenNextElementIsDestination(origin, destination, nextElement);
            }

            // Nếu nextElement là current AffectNode
            if (nextElement.equals(currentAffectNode)) {
                return WhenNextElementIsCurrentAffectNode(origin, destination, affectNodes, nextElement);
            }

            // Nếu nextElement dậm chân tại chỗ, tăng penalty
            if (origin instanceof Edge.RuntimeEdge) {
                if (nextElement.equals(((Edge.RuntimeEdge) origin).getSourceVertex())) {
                    penalty++;

                    // Dậm chân 10 lần thì chuyển sang affectNode tiếp theo
                    if (penalty > 5) {
                        currentAffectNodeIndex++;
                        penalty = 0;

                        // Hết affectNode thì thôi chấp nhận kẹt
                        if (currentAffectNodeIndex >= affectNodes.size()) {
                            currentAffectNodeIndex = 0;
                            currentDestination = null;
                            currentAffectNode = null;
                            isTraversingToAffectNode = false;
                            isTraversingToAnything = false;
                            logStatus(origin, destination, null);
                            return null;
                        }

                        currentAffectNode = affectNodes.get(currentAffectNodeIndex);
                        nextElement = aStar.getNextElement(origin, currentAffectNode);
                        logStatus(origin, destination, nextElement);
                        return nextElement;
                    }
                }
            }

            // Nếu không tức là vẫn tiếp tục đi đến affect Node bằng BFS, điểm yếu là nếu affectNode cũng đang khóa thì dậm chân tại chỗ
            logStatus(origin, destination, nextElement);
            return nextElement;
        }

        // Nếu không tức là đang đến destination đã được mở khóa
        Element nextElement = aStar.getNextElement(origin, currentDestination);

        // Nếu next Element là destination
        if (nextElement.equals(currentDestination)) {
            return WhenNextElementIsDestination(origin, destination, nextElement);
        }
        logStatus(origin, destination, nextElement);
        return nextElement;
    }

    private Element WhenNextElementIsCurrentAffectNode(Element origin, Edge.RuntimeEdge destination, ArrayList<Edge.RuntimeEdge> affectNodes, Element nextElement) {
        // Lấy giá trị các context variables để phục vụ roll back nếu như visit được affect Node mà k mở khóa được guard
        Set<String> contextVariables = polyglotContext.getBindings("js").getMemberKeys();
        Map<String, Value> contextVariablesMap = new HashMap<>();
        for (String contextVariable : contextVariables) {
            if (contextVariable.equals("JsonContext")) continue;
            contextVariablesMap.put(contextVariable, polyglotContext.getBindings("js").getMember(contextVariable));
        }

        // Thực thi các action của affect Node đã tìm thấy
        nextElement.getActions().forEach(action ->
            polyglotContext.eval("js", action.getScript()));

        // Nếu guard mở, oke vẫn roll back để tí nữa Execution Context sẽ execute lại
        boolean isGuardAvailable = polyglotContext.eval("js", currentDestination.getGuard().getScript()).asBoolean();
        if (isGuardAvailable) {
            return WhenNextElementIsCurrentAffectNodeThatUnlockDestinationGuard(origin, destination, nextElement, contextVariables, contextVariablesMap);
        }

        // Nếu guard không mở thì rollback state và chuyển sang visit affectNode khác
        return WhenNextElementIsCurrentAffectNodeThatDestinationGuardStillLocked(origin, destination, affectNodes, nextElement, contextVariables, contextVariablesMap);
    }


    private Element WhenNextElementIsCurrentAffectNodeThatDestinationGuardStillLocked(Element origin,
                                                                                      Edge.RuntimeEdge destination,
                                                                                      ArrayList<Edge.RuntimeEdge> affectNodes,
                                                                                      Element nextElement,
                                                                                      Set<String> contextVariables,
                                                                                      Map<String, Value> contextVariablesMap) {
        penalty = 0;
        currentAffectNodeIndex++;
        // roll back các bindings
        for (String contextVariable : contextVariables) {
            if (contextVariable.equals("JsonContext")) continue;
            polyglotContext.getBindings("js").putMember(contextVariable, contextVariablesMap.get(contextVariable));
        }
        // Nếu hết affectNode khác, trả null ra ngoài để detect
        if (currentAffectNodeIndex >= affectNodes.size()) {
            currentAffectNodeIndex = 0;
            currentDestination = null;
            currentAffectNode = null;
            isTraversingToAffectNode = false;
            isTraversingToAnything = false;
            penalty = 0;
            logStatus(origin, destination, null);
            return null;
        }

        // Chuyển sang affectNode khác
        currentAffectNode = affectNodes.get(currentAffectNodeIndex);
        nextElement = aStar.getNextElement(origin, currentAffectNode);
        logStatus(origin, destination, nextElement);
        return nextElement;
    }

    @NotNull
    private Element WhenNextElementIsCurrentAffectNodeThatUnlockDestinationGuard(Element origin,
                                                                                 Edge.RuntimeEdge destination,
                                                                                 Element nextElement,
                                                                                 Set<String> contextVariables,
                                                                                 Map<String, Value> contextVariablesMap) {
        isTraversingToAffectNode = false;
        currentAffectNode = null;
        currentAffectNodeIndex = 0;
        penalty = 0;
        // roll back các bindings
        for (String contextVariable : contextVariables) {
            if (contextVariable.equals("JsonContext")) continue;
            polyglotContext.getBindings("js").putMember(contextVariable, contextVariablesMap.get(contextVariable));
        }
        logStatus(origin, destination, nextElement);
        return nextElement;
    }

    private Element WhenNextElementIsDestination(Element origin, Edge.RuntimeEdge destination, Element nextElement) {
        isTraversingToAffectNode = false;
        isTraversingToAnything = false;
        currentAffectNodeIndex = 0;
        currentAffectNode = null;
        currentDestination = null;
        penalty = 0;
        logStatus(origin, destination, nextElement);
        return nextElement;
    }

    private void logStatus(Element origin, Edge.RuntimeEdge destination, Element nextElement) {
        String currentAffectNodeName = currentAffectNode != null ? currentAffectNode.getName() : null;
        String currentDestinationName = currentDestination != null ? currentDestination.getName() : null;
        String nextElementName = nextElement != null ? nextElement.getName() : null;
        LOG.debug("Current MySearch status: \n" +
                "- origin is {} \n" +
                "- destination is {} \n" +
                "- currentDestination is {} \n" +
                "- isTraversingToAnything is {} \n" +
                "- isTraversingAffectNode is {} \n" +
                "- currentAffectNode is {} \n" +
                "- currentAffectNodeIndex is {} \n" +
                "- penalty is {} \n" +
                "- affectMatrixMap is {} \n" +
                "- nextElement is {} ",
            origin.getName(), destination.getName(), currentDestinationName, isTraversingToAnything,
            isTraversingToAffectNode, currentAffectNodeName,
            currentAffectNodeIndex, penalty, Arrays.toString(affectMatrixMap.get(destination).toArray()), nextElementName);
    }
}
