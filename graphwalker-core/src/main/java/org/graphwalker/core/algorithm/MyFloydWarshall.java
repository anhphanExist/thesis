package org.graphwalker.core.algorithm;

import org.graalvm.polyglot.Value;
import org.graphwalker.core.generator.QuickRandomPath;
import org.graphwalker.core.machine.Context;
import org.graphwalker.core.model.Edge;
import org.graphwalker.core.model.Element;
import org.graphwalker.core.model.Model;
import org.graphwalker.core.model.Vertex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class MyFloydWarshall implements Algorithm {

  private static final Logger LOG = LoggerFactory.getLogger(MyFloydWarshall.class);
  private final Context context;
  private final Model.RuntimeModel model;
  private int[][] distancesForGuards;

  public MyFloydWarshall(Context context) {
    this.context = context;
    this.model = context.getModel();
    // Đặt khoảng cách giữa các phần tử cạnh nhau là 1
//    this.distances = createDistanceMatrix(model, model.getElements());
    // Ma trận phần tử có điều kiện
    this.distancesForGuards = createDistanceMatrixForGuards(model, model.getElements());
    // Tạo ma trận khoảng cách giữa các phần tử xa nhau trong mô hình
    createPredecessorMatrix(model.getElements(), distancesForGuards);
  }

  public void reCalculate() {
    distancesForGuards = createDistanceMatrixForGuards(model, model.getElements());
    createPredecessorMatrix(model.getElements(), distancesForGuards);
  }

  private Element[][] createPredecessorMatrix(List<Element> elements, int[][] distances) {
    Element[][] predecessors = new Element[elements.size()][elements.size()];
    int size = elements.size();
    for (int k = 0; k < size; k++) {
      for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
          // Nếu đã tồn tại d(i -> k)
          // và đã tồn tại d(k -> j)
          // và d(i -> k) + d(k -> j) < d(i -> j)
          // thì cập nhật d(i -> j) = d(i -> k) + d(k -> j)
          // và gán kế nhiệm của i -> j là node k
          if (distances[i][k] != Integer.MAX_VALUE
            && distances[k][j] != Integer.MAX_VALUE
            && distances[i][k] + distances[k][j] < distances[i][j]) {
            distances[i][j] = distances[i][k] + distances[k][j];
            predecessors[i][j] = elements.get(k);
          }
        }
      }
    }
    return predecessors;
  }

  public int getShortestDistance(Element origin, Element destination) {
    if (!model.getElements().contains(destination)) {
      return Integer.MAX_VALUE;
    } else if (origin.equals(destination)) {
      return 0;
    } else {
      return distancesForGuards[model.getElements().indexOf(origin)][model.getElements().indexOf(destination)];
    }
  }

  public int getMaximumDistance(Element destination) {
    int maximumDistance = Integer.MIN_VALUE;
    for (int[] distance : distancesForGuards) {
      int value = distance[model.getElements().indexOf(destination)];
      if (value != Integer.MAX_VALUE && value > maximumDistance) {
        maximumDistance = value;
      }
    }
    return maximumDistance;
  }

  private int[][] createDistanceMatrixForGuards(Model.RuntimeModel model, List<Element> elements) {
    int[][] distancesForGuards = new int[elements.size()][elements.size()];
    for (int[] row : distancesForGuards) {
      Arrays.fill(row, Integer.MAX_VALUE);
    }
    for (Element element : elements) {
      // Nếu element là edge => đặt khoảng cách giữa cạnh đó với điểm đến của cạnh đó bằng 0
      // Nếu element là vertex => đặt khoảng cách giữa đỉnh đó với các cạnh từ nó đi ra mặc định bằng 20
      if (element instanceof Edge.RuntimeEdge) {
        Edge.RuntimeEdge edge = (Edge.RuntimeEdge) element;
        Vertex.RuntimeVertex targetVertex = edge.getTargetVertex();
        distancesForGuards[elements.indexOf(edge)][elements.indexOf(targetVertex)] = 0;
      } else if (element instanceof Vertex.RuntimeVertex) {
        Vertex.RuntimeVertex vertex = (Vertex.RuntimeVertex) element;
        for (Edge.RuntimeEdge edge : model.getOutEdges(vertex)) {
          // Trọng số mặc định
          distancesForGuards[elements.indexOf(vertex)][elements.indexOf(edge)] = 20;

          // Nếu có action thì trọng số = 1/10
          if (edge.hasActions()) {
            LOG.debug("GUARD '{}' IN METHOD '{}' HAS ACTIONS !!  SET distance between '{}' and '{}' = 2",
              edge.getGuard().getScript(), edge.getName(), vertex.getName(), edge.getName());
            distancesForGuards[elements.indexOf(vertex)][elements.indexOf(edge)] = 2;
          }

          // Nếu edge có guard, guard có feasible thì trọng số = 1/20, không thì penalty x100
          if (edge.hasGuard()) {
            org.graalvm.polyglot.Context polyglotContext = context.getExecutionEnvironment();
            LOG.debug("MyFloydWarshall Execute guard: '{}' in edge '{}' in model: '{}'", edge.getGuard().getScript(), edge.getName() , context.getModel().getName());
            boolean isGuardFeasible = polyglotContext.eval("js", edge.getGuard().getScript()).asBoolean();
            if (isGuardFeasible) {
              LOG.debug("GUARD '{}' IN METHOD '{}' IS UNLOCKED !!  SET distance between '{}' and '{}' = 1",
                edge.getGuard().getScript(), edge.getName(), vertex.getName(), edge.getName());
              distancesForGuards[elements.indexOf(vertex)][elements.indexOf(edge)] = 1;
            } else {
              LOG.debug("GUARD '{}' IN METHOD '{}' IS LOCKED !! SET distance between '{}' and '{}' = 2000",
                edge.getGuard().getScript(), edge.getName(), vertex.getName(), edge.getName());
              distancesForGuards[elements.indexOf(vertex)][elements.indexOf(edge)] = 2000;
            }
          }

          // penalty x5 cho edge loop tại chỗ mặc định
          Vertex.RuntimeVertex targetVertex = edge.getTargetVertex();
          if (vertex.equals(targetVertex)) {
            distancesForGuards[elements.indexOf(vertex)][elements.indexOf(edge)] *= 5;
          }
        }
      }
    }
    return distancesForGuards;
  }
}
