package org.graphwalker.core.algorithm;

import org.graphwalker.core.machine.Context;
import org.graphwalker.core.model.Element;
import org.graphwalker.core.model.Path;

import java.util.*;

import static org.graphwalker.core.common.Objects.isNotNull;
import static org.graphwalker.core.common.Objects.isNull;

public class MyAStar implements Algorithm {
  private final Context context;

  public MyAStar(Context context) {
    this.context = context;
  }

  public Element getNextElement(Element origin, Element destination) {
    // Khởi tạo danh sách các nút đã từng được tính toán nhưng chưa duyệt qua
    Map<Element, MyNode> openSet = new HashMap<>();

    // Khởi tạo hàng đợi ưu tiên
    PriorityQueue<MyNode> queue = new PriorityQueue<>(10, new MyNodeComparator());

    // Khởi tạo danh sách các nút đã được duyệt qua
    Map<Element, MyNode> closeSet = new HashMap<>();

    // Lấy thuật toán Floyd Warshall làm hàm heuristic, tính toán lại ma trận khoảng cách
    MyFloydWarshall myFloydWarshall = context.getAlgorithm(MyFloydWarshall.class);
    myFloydWarshall.reCalculate();

    // Khởi tạo nút Astar tương ứng với element hiện tại
    MyNode sourceNode = new MyNode(origin, 0, myFloydWarshall.getShortestDistance(origin, destination));

    // Ánh xạ astar node với element hiện tại
    openSet.put(origin, sourceNode);

    // add element hiện tại vào hàng đợi ưu tiên
    queue.add(sourceNode);

    // pop element hiện tại khỏi hàng đợi ưu tiên
    MyNode node = queue.poll();
    if (node == null) {
      throw new AlgorithmException();
    }

    // Nếu element hiện tại là đích đến, trả về element hiện tại
    // Nếu không, đánh dấu element hiện tại đã được duyệt qua, lấy neighbors của element hiện tai vào tính toán
    if (node.getElement().equals(destination)) {
      return node.getElement();
    } else {
      closeSet.put(node.getElement(), node);
      List<Element> neighbors = context.filter(context.getModel().getElements(node.getElement()));
      calculate(destination, openSet, queue, closeSet, myFloydWarshall, node, neighbors);
    }

    // Nếu hàng đợi vẫn còn phần tử, trả về phần tử ưu tiên tiếp theo
    if (!queue.isEmpty()) {
      MyNode result = queue.poll();
      if (null != result) {
        return result.getElement();
      }
    }
    throw new AlgorithmException();
  }

  private void calculate(Element destination, Map<Element, MyNode> openSet, PriorityQueue<MyNode> queue, Map<Element, MyNode> closeSet,
                         MyFloydWarshall myFloydWarshall, MyNode node, List<Element> neighbors) {
    // Duyệt các neighbor của nút hiện tại
    for (Element neighbor : neighbors) {
      MyNode visited = closeSet.get(neighbor);

      // Nếu neighbor chưa được duyệt qua mới tính toán cho neighbor đó
      if (isNull(visited)) {
        // Tính toán chi phí áng chừng bằng heuristic của neighbor này
        double g = node.getG() + myFloydWarshall.getShortestDistance(node.getElement(), neighbor);

        // Nếu neighbor mới nguyên, thêm nó vào hàng đợi và open set
        // Nếu neighbor của neighbor đã từng được tính toán nhưng chưa từng duyệt qua, có thể cập nhật lại trọng số và ước lượng
        MyNode neighborNode = openSet.get(neighbor);
        if (isNull(neighborNode)) {
          neighborNode = new MyNode(neighbor, g, myFloydWarshall.getShortestDistance(neighbor, destination));
          neighborNode.setParent(node);
          openSet.put(neighbor, neighborNode);
          queue.add(neighborNode);
        } else if (g < neighborNode.getG()) {
          neighborNode.setParent(node);
          neighborNode.setG(g);
          neighborNode.setH(myFloydWarshall.getShortestDistance(neighbor, destination));
        }
      }
    }
  }

  public Path<Element> getShortestPath(Element origin, Element destination) {
    Map<Element, MyNode> openSet = new HashMap<>();
    PriorityQueue<MyNode> queue = new PriorityQueue<>(10, new MyNodeComparator());
    Map<Element, MyNode> closeSet = new HashMap<>();
    MyFloydWarshall myFloydWarshall = context.getAlgorithm(MyFloydWarshall.class);
    MyNode sourceNode = new MyNode(origin, 0, myFloydWarshall.getShortestDistance(origin, destination));
    openSet.put(origin, sourceNode);
    queue.add(sourceNode);
    MyNode targetNode = null;
    while (openSet.size() > 0) {
      MyNode node = queue.poll();
      if (null != node) {
        openSet.remove(node.getElement());
        if (node.getElement().equals(destination)) {
          targetNode = node;
          break;
        } else {
          closeSet.put(node.getElement(), node);
          List<Element> neighbors = context.filter(context.getModel().getElements(node.getElement()));
          calculate(destination, openSet, queue, closeSet, myFloydWarshall, node, neighbors);
        }
      }
    }
    if (isNotNull(targetNode)) {
      List<Element> path = new ArrayList<>();
      path.add(targetNode.getElement());
      MyNode node = targetNode.getParent();
      while (isNotNull(node)) {
        path.add(node.getElement());
        node = node.getParent();
      }
      Collections.reverse(path);
      return new Path<>(path);
    }
    throw new AlgorithmException();
  }

  private class MyNode {

    private final Element element;
    private MyNode parent;
    private double g;
    private double h;

    MyNode(Element element, double g, double h) {
      this.element = element;
      this.g = g;
      this.h = h;
    }

    private Element getElement() {
      return element;
    }

    private MyNode getParent() {
      return parent;
    }

    private void setParent(MyNode parent) {
      this.parent = parent;
    }

    private double getG() {
      return g;
    }

    private void setG(double g) {
      this.g = g;
    }

    private void setH(double h) {
      this.h = h;
    }

    public double getF() {
      return g + h;
    }
  }

  private class MyNodeComparator implements Comparator<MyNode> {

    public int compare(MyNode first, MyNode second) {
      if (first.getF() < second.getF()) {
        return -1;
      } else if (first.getF() > second.getF()) {
        return 1;
      } else {
        return 0;
      }
    }
  }
}
